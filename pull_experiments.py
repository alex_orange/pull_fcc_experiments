import re

import bs4
import requests

import experiment


form_data = {
	"frm-search": "frm-search",
	"frm-search:applicationType": [
		"M",
		"P"
	],
	"frm-search:dateReceiptBegin": "",
	"frm-search:dateReceiptTo": "",
	"frm-search:emission": "",
	"frm-search:erpFrom": "",
	"frm-search:erpTo": "",
	"frm-search:erpUnit": "",
	"frm-search:eirpFrom": "",
	"frm-search:eirpTo": "",
	"frm-search:eirpUnit": "",
	"frm-search:experimentType": "",
	"frm-search:fileNo": "",
	"frm-search:freqFrom": "",
	"frm-search:freqTo": "",
	"frm-search:frequencyUnit": "",
	"frm-search:scopeOfService": "",
	"search-criteria": "select",
	"search-criteria-gis": "select",
	"frm-search:appName": "",
	"frm-search:callSign": "",
	"frm-search:btn-search": "Search",
	"frm-search:location_json": "",
	"frm-search:buffer": "",
	"frm-search:sel-displayOpts1": "All",
	"frm-search:sel-sort": "date",
	"frm-search:sortOrder": "asc",
	"javax.faces.ViewState": "-3864100652217775463:-1374240007937318418"
}

fcc_url = "https://apps2.fcc.gov/ELSExperiments/pages/experimentSearch.htm"

def get_experiment_page_by_call_sign(call_sign):
    form_data['frm-search:callSign'] = call_sign

    s = requests.Session()

    r = s.get(fcc_url)

    soup = bs4.BeautifulSoup(r.text, features="html.parser")

    view_state = soup.find(attrs={"name": "javax.faces.ViewState"})
    form_data["javax.faces.ViewState"] = view_state.attrs["value"]

    r = s.post(fcc_url, data=form_data)

    return r.text

def parse_experiments_from_page(page):
    soup = bs4.BeautifulSoup(page, features="html.parser")

    experiments = []

    for section in soup.find_all("section"):
        call_sign = section.find(string="Call sign:").parent.parent.next_sibling
        call_sign = call_sign.strip()

        experiment_id = section.find(string="Experiment ID:").parent.parent
        experiment_id = experiment_id.next_sibling.strip()

        operating_days_hours = section.find(string="Operating days, hours:")
        operating_days_hours = operating_days_hours.parent.next_sibling.strip()

        status = section.find(string=re.compile(r"Status:")).split("\xa0")[1]

        frequency_ranges = []

        operating_frequencies = section.find(string="Operating frequencies:")
        frequencies_str = operating_frequencies.parent.next_sibling
        frequencies_str = frequencies_str.strip()

        frequency_parts = frequencies_str.split(" ")
        num_frequency_parts = len(frequency_parts)
        assert num_frequency_parts % 4 == 0

        num_frequencies = num_frequency_parts / 4

        for i in range(0, num_frequency_parts, 4):
            assert frequency_parts[i+1] == '-'
            assert frequency_parts[i+3] == 'MHz'

            frequency_ranges.append((float(frequency_parts[i]),
                                     float(frequency_parts[i+2])))

        experiments.append(experiment.Experiment(call_sign,
                                                 experiment_id,
                                                 frequency_ranges,
                                                 operating_days_hours,
                                                 status))

    return experiments
