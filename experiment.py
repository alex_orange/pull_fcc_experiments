import dataclasses
import typing


@dataclasses.dataclass
class Experiment:
    call_sign: str
    experiment_id: str
    frequency_ranges: typing.List[typing.Tuple[float, float]]
    operating_days_hours: str
    status: str
