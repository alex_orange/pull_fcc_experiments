import pull_experiments

page = pull_experiments.get_experiment_page_by_call_sign("WA3XAV")
experiments = pull_experiments.parse_experiments_from_page(page)

for experiment in experiments:
    print(experiment)
    for frequency_range in experiment.frequency_ranges:
        print(frequency_range)

page = pull_experiments.get_experiment_page_by_call_sign("WA3XBO")
experiments = pull_experiments.parse_experiments_from_page(page)

for experiment in experiments:
    print(experiment)
    for frequency_range in experiment.frequency_ranges:
        print(frequency_range)
